package com.example.demo.controllers;

import com.example.demo.models.PersonModel;
import com.example.demo.models.TaskModel;
import com.example.demo.repositories.PersonRepo;
import com.example.demo.repositories.TaskModelRepo;
import com.example.demo.services.AllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

@RestController
public class Controllers {
    @Autowired
    public AllService allService;
    @Autowired
    public TaskModelRepo taskModelRepo;
    @Autowired
    public PersonRepo personRepo;

    @PostMapping("createPerson")
    public ResponseEntity addPerson(@RequestParam("name") String name, @RequestParam("PN") Long pm) throws IOException {
        PersonModel model = new PersonModel();
        model.setName(name);
        model.setPersonalNumber(pm);
        allService.addPerson(model);



        return ResponseEntity.ok("ok");
    }
    @PostMapping("createTask")
    public ResponseEntity addTask(@RequestParam("name") String name, @RequestParam("id") Long id) throws IOException {
        TaskModel model = new TaskModel();
        model.setName(name);
        model.setAuthorId(id);
        allService.addTask(model);
        PersonModel pModel = personRepo.findById(id).get();
        pModel.getTasks().add(model);
        personRepo.save(pModel);
        return ResponseEntity.ok("ok");
    }

    @GetMapping("read")
    public String read (@RequestParam("id") Long id) throws IOException {
        PersonModel model = personRepo.findById(id).get();


        List<TaskModel> ss= model.getTasks();


        System.out.println(ss);
        return model.getName() + "\n"
                + model.getTasks().get(0).getName() + "\n"
                + model.getTasks().get(1).getName() + "\n"
                + model.getTasks().get(2).getName();

    }

    @PostMapping("update")
    public ResponseEntity upInfo(@RequestParam("id") Long id, @RequestParam("name") String name, @RequestParam("contain") String contain) throws IOException, NullPointerException{
        return ResponseEntity.ok("ok");

    }

    @DeleteMapping("delete")
    public ResponseEntity delInfo (@RequestParam("id") Long id) throws IOException {
        return ResponseEntity.ok("ok");

    }


}
