package com.example.demo.models;

import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Person")
public class PersonModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long Id;
    @Column(name = "P_PERSONAL_NUM")
    Long personalNumber;
    @Column(name = "P_SURNAME")
    String surname;
    @Column(name = "P_NAME")
    String name;
    @Column(name = "P_PATRONYMIC")
    String patronymic;
    @Column(name = "P_ROLE")
    String role;
    @OneToMany
    @JoinColumn(name = "authorId", referencedColumnName = "Id")
    List<TaskModel> tasks;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Long getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(Long personalNumber) {
        this.personalNumber = personalNumber;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<TaskModel> getTasks() {
        return tasks;
    }

    public void setTasks(List<TaskModel> tasks) {
        this.tasks = tasks;
    }
}
