package com.example.demo.models;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "TaskPerson")
public class TaskPersonModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    Long task_id;
    Long Person_id;
    String task_role;
}
