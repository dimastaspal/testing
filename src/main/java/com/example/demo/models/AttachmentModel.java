package com.example.demo.models;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Attachment")
public class AttachmentModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long Id;
    Long Task_id;
    Long comment_id;
    Long file_id;
}
