package com.example.demo.services;

import com.example.demo.models.PersonModel;
import com.example.demo.models.TaskModel;
import com.example.demo.repositories.PersonRepo;
import com.example.demo.repositories.TaskModelRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AllService {
    @Autowired
    PersonRepo personRepo;
    @Autowired
    TaskModelRepo taskModelRepo;

    public boolean addPerson(PersonModel model) {
        personRepo.save(model);
        if (personRepo.existsById(model.getId())){
            return true;
        } else {
            return false;
        }
    }

    public boolean addTask(TaskModel model) {
        taskModelRepo.save(model);
        if (taskModelRepo.existsByTaskId(model.getTaskId())){
            return true;
        } else {
            return false;
        }
    }
}
