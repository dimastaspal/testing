package com.example.demo.repositories;

import com.example.demo.models.PersonModel;
import com.example.demo.models.TaskModel;
import com.example.demo.models.TaskPersonModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PersonRepo extends JpaRepository<PersonModel, Long> {

    Optional<PersonModel> findById(Long id);

    PersonModel findByName(String name);
    boolean existsById(Long fileId);
}
