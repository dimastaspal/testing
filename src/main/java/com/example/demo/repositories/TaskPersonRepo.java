package com.example.demo.repositories;

import com.example.demo.models.TaskModel;
import com.example.demo.models.TaskPersonModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TaskPersonRepo extends JpaRepository<TaskPersonModel, Long> {

  //  Optional<TaskPersonModel> findById(Long id);
 //   TaskPersonModel findByTaskPersonId(String fileId);
//    boolean existsByTaskPersonId(String fileId);
}
