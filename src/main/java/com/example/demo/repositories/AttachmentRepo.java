package com.example.demo.repositories;

import com.example.demo.models.AttachmentModel;
import com.example.demo.models.CommentModel;
import com.example.demo.models.TaskModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AttachmentRepo extends JpaRepository<AttachmentModel, Long> {

   // Optional<AttachmentModel> findById(Long id);
  //  AttachmentModel findByAttachmentId(String fileId);
 //   boolean existsByAttachmentId(String fileId);
}
