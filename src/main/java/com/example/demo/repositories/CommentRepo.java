package com.example.demo.repositories;

import com.example.demo.models.CommentModel;
import com.example.demo.models.PersonModel;
import com.example.demo.models.TaskModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CommentRepo extends JpaRepository<CommentModel, Long> {

   // Optional<CommentModel> findById(Long id);
   // CommentModel findByCommentId(String fileId);
 //   boolean existsByCommentId(String fileId);
}
