package com.example.demo.repositories;

import com.example.demo.models.TaskModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TaskModelRepo extends JpaRepository<TaskModel, Long> {

    Optional<TaskModel> findById(Long id);
    TaskModel findByTaskId(String fileId);
    boolean existsByTaskId(Long fileId);

}
